
(function() {
	
	Ti.include("common/GlobalVars.js");
	
	//Ti.App.fireEvent("Before db");
	Ti.API.log("HERE");
	var db = Ti.Database.install('data/KateDb.sqlite', 'KateDb');
	
	db.file.remoteBackup = false;
	
	// Check db for existing records
	var checkLast = db.execute("SELECT id FROM articles");
	
	
	Ti.App.addEventListener("loadUI", function(){
		
		var tb = new AppTabGroup().open();	
	  	
		Ti.App.customTabGroup = tb; 
	  	
	});
	
	
	// had problems with syncing on 3G so only do load sync on wifi unless new install
	if ( Titanium.Network.networkType == Titanium.Network.NETWORK_WIFI || checkLast.rowCount == 0 ){ 
		Ti.include("data/sync.js");
		Ti.App.syncBack = "loadUI";
		Ti.API.fireEvent('sync_categories');
	}else{
		Ti.App.fireEvent('loadUI');
	}
	
	// Close down db
	checkLast.close();
	db.close();
	
})();