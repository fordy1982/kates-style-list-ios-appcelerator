
Ti.API.log("Start of article list");

	// ========================================
	// Page Setup Variables
	// ========================================
	Ti.include("../common/GlobalVars.js");
	var db;
	var self = Ti.UI.currentWindow;
	self.backgroundImage = "../images/background.png";
	
	// Create a table view
	var article_table = Ti.UI.createTableView({
				backgroundColor:'transparent',
				top:	5,
				separatorStyle: Titanium.UI.iPhone.TableViewSeparatorStyle.NONE
				
			});	 
		
	self.add(article_table);
	//self.add(ad);
	
	// Create header for pull down
	var refreshHeader = Ti.UI.createView({
		width:320,
		height:60
	});
	//backgroundImage: "../images/pulldown.png",
	
	var arrow = Ti.UI.createView({
		backgroundImage:"../images/arrowRefresh.png",
		width:18,
		height:32,
		bottom:10,
		left:20
	});
	
	
	var statusLabel = Ti.UI.createLabel({
		text:"Pull to reload",
		left:55,
		width:200,
		bottom:30,
		height:"auto",
		color:"#ffffff",
		textAlign:"center",
		font:{fontSize:13,fontWeight:"bold"}
	});
	
	
	var lastUpdatedLabel = Ti.UI.createLabel({
		text:"Last Updated: "+formatDate(),
		left:55,
		width:200,
		bottom:15,
		height:"auto",
		color:"#ffffff",
		textAlign:"center",
		font:{fontSize:12}
	});
	
	
	var actInd = Ti.UI.createActivityIndicator({
		left:20,
		bottom:13,
		width:30,
		height:30
	});
	
	refreshHeader.add(arrow);
	refreshHeader.add(statusLabel);
	refreshHeader.add(lastUpdatedLabel);
	refreshHeader.add(actInd);	
	
	article_table.headerPullView = refreshHeader;
	
	var pulling = false;
	var reloading = false;
	var updating = false;
	var articles;
	var lastRow;
	var lastTimestamp;
	var loadingRow = Ti.UI.createTableViewRow({title:"Loading..."});
	
	// ========================================
	// Page Controller Logic
	// ========================================
	function loadPage(rl){
		
		// Set to 0 to reset for pull down refresh
		lastTimestamp = 0;
		
		// Look up the latest articles
		articles = getArticles();
		
		if (isIPhone || isIPad){
			iPhoneView(articles);
		}
		
	}
	
	
	function getArticles(before, limit){
		
		db = Ti.Database.open('KateDb');
		
		if ( before != null){
			before = " WHERE timestamp < " + before;
		} else {
			before = "";
		} 
		
		if ( limit == null) {
			limit = 10;
		}
		
		return db.execute("SELECT a.id, a.server_id, a.headline, a.intro, a.favorite, a.featured, a.date, a.timestamp, a.date_created, a.thumbnail_path FROM articles a "+ before +" ORDER BY a.timestamp DESC LIMIT ?", limit);
	}
	
	
	// ========================================
	// Page View
	// ========================================
	function iPhoneView(data){
		
		var articlesData = [];
		
		if ( data.rowCount > 0 ) {
			
			lastRow = data.rowCount - 1;
			Ti.API.log("lastRow: " + lastRow);
			
			// Got Data
			while ( data.isValidRow() ) {
			
				var row = createRow(data);
							
				articlesData.push(row);		
				
				data.next();		
				
			}	 
			
		} else {
			// No Data
			var row = Ti.UI.createTableViewRow({						
						id: 			0,
						rowHeight: 		30,
						hasChild: 		false,
						touchEnabled: 	false,
						top:			5,
						left:			5,
						right:			5,
						backgroundColor:'transparent',
						selectedBackgroundColor: "transparent"
					});
					
			var itemLabel = Ti.UI.createLabel({
								text:	"No Results...",
								font:		{ fontSize:16,fontWeight:'bold' },
								left:	10,
								width: 	300,
								height:	30
							});
							
			row.add(itemLabel);
							
			articlesData.push(row);	
			
		}
		
		article_table.setData(articlesData);
		
		db.close();
		
	}
	
	
	function createRow(data) {
		
		var row = Ti.UI.createTableViewRow({						
					id: 			data.fieldByName("id"),
					server_id:		data.fieldByName("server_id"),
					rowHeight: 		100,
					hasChild: 		true,
					touchEnabled: 	true,
					url:			"ArticlePage.js",
					top:			5,
					left:			5,
					rightImage:		"../images/arrow.png",
					selectedBackgroundColor: "transparent"
					
				});
		
		var img = "/images/missingThumb.png";
		
		if ( data.fieldByName("thumbnail_path") ){
			imgFile = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory+'../Library/Caches/', data.fieldByName("thumbnail_path"));
			Ti.API.log("Image exists");
			if ( imgFile.exists() ){
				
				img = imgFile;
			}
			
		}
		
		
		var itemThumb = Ti.UI.createImageView({
						image: img,
						left:	5,
						width: 	100,
						height:	100
					});
						
		
		var contentView = Ti.UI.createView({
			layout:	"vertical",
			left:	110,
			width:	205,
			height: 100,
			backgroundColor: "#012547",
			selectedBackgroundColor: "#012547"
		});
		
		
		var itemLabel = Ti.UI.createLabel({
						text:	data.fieldByName("headline"),
						font:	{ fontSize:14,fontWeight:'bold' },
						color: "#ffffff",
						top: 	5,
						left:	5,
						width: 	170
					});
		
		contentView.add(itemLabel);			
		
		var itemDate = Ti.UI.createLabel({
						text:	data.fieldByName("date"),
						font:	{ fontSize:10, fontStyle: "italic" },
						color:  "#ffffff",
						left:	5
					});
					
		if (data.fieldByName("favorite") == 1 ) {
						
			var favTag = Ti.UI.createImageView({
							image:'/images/favTag.png',
							right:	0,
							top:	3,
							width: 	11,
							height:	20,
							zIndex: 99
						});
			
			row.add(favTag);
									
		} 
		
		
		// Get categories
		
		lastTimestamp = data.fieldByName('timestamp');
		
		contentView.add(itemDate);
		
		row.add(itemThumb);
		
		row.add(contentView);
		
		return row;
		
	}
	
	
	
	
	// Function to set a date string for the pull down
	// TODO:  Check whether this can be switched out for the function in GlobalVars
	// returns: String
	function formatDate() {
		var date = new Date();
		var datestr = date.getMonth()+'/'+date.getDate()+'/'+date.getFullYear();
		if (date.getHours()>=12) {
			datestr+=' '+(date.getHours()==12 ? date.getHours() : date.getHours()-12)+':'+date.getMinutes()+' PM';
		} else {
			datestr+=' '+date.getHours()+':'+date.getMinutes()+' AM';
		}
		return datestr;
	}
	
	
	
	// ========================================
	// Event Listeners
	// ========================================
	
	// Setup the page each time the window is shown
	// Doing this so the content is always up to date from the local storage
	self.addEventListener("open", function(){
		loadPage();
	});
	
	self.addEventListener("postlayout", function(){
		//db.close();
	});
	
	
	// ========================================
	// Pull to reload function
	// ========================================
	
	// Called from the pull down
	self.addEventListener('beginReloading', function(e){
		
		// Get sync script
		//Ti.include("/data/sync.js");
		
		// Set global var so the sync script knows where to call back to
		Ti.App.syncBack = "endReloading";
		
		// Do sync
		Ti.API.fireEvent('full_sync');
		
	});
	
	
	// Call back function for the sync script to fire an event for.
	var endReloading = function(e){
		Ti.API.log("In endReloading");
		// when you're done, just reset
		article_table.setContentInsets({top:0},{animated:true});
		reloading = false;
		updating = false;
		lastUpdatedLabel.text = "Last Updated: "+formatDate();
		statusLabel.text = "Pull down to refresh...";
		actInd.hide();
		//arrow.show();
		
		// Reset data on the page
		loadPage();
		
		//Ti.App.removeEventListener("end_reloading", endReloading);
		
	};
	
	
	// Catch release of the scroll at the point we should refresh the page
	article_table.addEventListener('scrollEnd', function(e){	
		 if(pulling && !reloading){
			reloading = true;
			pulling = false;
			arrow.hide();
			actInd.show();
			statusLabel.text = "Reloading...";
			article_table.setContentInsets({top:60},{animated:true});
			article_table.scrollToTop(-60,true);
			arrow.transform=Ti.UI.create2DMatrix();
			self.fireEvent("beginReloading");
		}
		
	});
	
	
	
	// ========================================
	// Update from bottom functions
	// ========================================
	var showing_loading = false;
	
	function beginUpdate(){
		
		updating = true;
		//navActInd.show();
	
		articles = getArticles(lastTimestamp);
		var ac = articles.rowCount;
		db.close();
		
		
		if ( ac < 20 ) {
			article_table.appendRow(loadingRow);
			
			showing_loading = true;
			Ti.App.syncBack = "end_updating";
			Ti.App.addEventListener('end_updating', updateFromSyncForUpdate);
			Ti.App.fireEvent("sync_from", {ts:lastTimestamp});
		}else{
			completeUpdate(lastTimestamp);	
		}		
		
	}
	
	
	
	var updateFromSyncForUpdate = function(){
		completeUpdate(lastTimestamp);
	};
	
	function completeUpdate(ts){
		
		Ti.App.removeEventListener('end_updating', updateFromSyncForUpdate);
		articles = getArticles(ts);
		
		if ( showing_loading == true ){
			article_table.deleteRow(lastRow+1);
			showing_loading = false;
		}
		
		if ( articles.rowCount > 0 ){
			while ( articles.isValidRow() ){
				
				article_table.appendRow(createRow(articles),{animationStyle:Titanium.UI.iPhone.RowAnimationStyle.NONE});
				lastRow += 1;
				
				articles.next();
				
			}
			
			/* 
			 article_table.animate({
		      top:-20,
		      duration:500
		    });
			*/
			
			//article_table.scrollToIndex(lastRow-1,{animated:true,position:Ti.UI.iPhone.TableViewScrollPosition.BOTTOM});
			//Ti.API.log("Index: " + article_table.getIndex());
		}
		
		db.close();
		
		//navActInd.hide();
		updating = false;
	}
	
	
	var lastDistance = 0; // calculate location to determine direction
	
	// Event listener to track the scroll of the page to enable
	// the Facebook style pull down to refresh
	article_table.addEventListener('scroll',function(e) {
		
		var offset = e.contentOffset.y;
		var height = e.size.height;
		var total = offset + height;
		var theEnd = e.contentSize.height;
		var distance = theEnd - total;
		

		if (offset < -65.0 && !pulling && !reloading){
			var t = Ti.UI.create2DMatrix();
			t = t.rotate(-180);
			pulling = true;
			arrow.animate({transform:t,duration:180});
			statusLabel.text = "Release to refresh...";
		} else if((offset > -65.0 && offset < 0 ) && pulling && !reloading) {
			pulling = false;
			var t = Ti.UI.create2DMatrix();
			arrow.animate({transform:t,duration:180});
			statusLabel.text = "Pull down to refresh...";
		} else if (distance < lastDistance){
			
			// going down is the only time we dynamically load,
			// going up we can safely ignore -- note here that
			// the values will be negative so we do the opposite
			
			// adjust the % of rows scrolled before we decide to start fetching
			var nearEnd = theEnd * .75;
			
			if (!updating && (total >= nearEnd))
			{
				beginUpdate();
			}
		}
		
		lastDistance = distance;
		   
	});
	
	
	// Event listener attached to the table view to catch row clicks
	article_table.addEventListener("click", function(e){
		
		// Check for a url so we know it is a valid article
		if ( e.rowData.url ) {
			var win = Titanium.UI.createWindow({
						url:				e.rowData.url,
						titleImage:			"../images/logo.png",
						backgroundImage:	'../images/background.png',
						
			});
			
			win.articleId = e.rowData.id;
			win.articleServerId = e.rowData.server_id;
		}
		
		Titanium.UI.currentTab.open(win,{animated:true});
	});
	
	
	// Close down memory
	self.addEventListener("close", function(){
		articles.close();
		db.close();
	});
	
	
	
	Ti.App.addEventListener('end_reloading', endReloading);
