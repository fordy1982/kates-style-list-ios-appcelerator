
	// ========================================
	// Page Setup Variables
	// ========================================
	Ti.include("../common/GlobalVars.js");
	
	var self = Ti.UI.currentWindow;
	
	var db = Ti.Database.open('KateDb');
	
	// Pulled the article_table out of the view because of issues with duplication on focus
	var article_table = Ti.UI.createTableView({
			backgroundColor:'transparent',
			top:	5,
			separatorStyle: Titanium.UI.iPhone.TableViewSeparatorStyle.NONE
			
		});	 
		
	self.add(article_table);
	//self.add(ad);
	
	// ========================================
	// Page Controller Logic
	// ========================================
	function loadPage(rl){
		
		var fSql = db.execute("SELECT id, headline, intro, favorite, featured, date, date_created, thumbnail_path FROM articles WHERE favorite = 1 ORDER BY date DESC ");
		
		iPhoneView(fSql);
		
	}
	

	// ========================================
	// Page View
	// ========================================
	function iPhoneView(articles){
		
		var articlesData = [];
		
		// Check for articles in the results passed to the view
		if (articles.rowCount > 0 ) {
			
			// Articles exist so loop through them and display
			while ( articles.isValidRow() ) {
				
				var row = Ti.UI.createTableViewRow({						
									id: 			articles.fieldByName("id"),
									rowHeight: 		100,
									hasChild: 		true,
									touchEnabled: 	true,
									url:			"ArticlePage.js",
									top:			5,
									left:			5,
									rightImage:		"../images/arrow.png",
									selectedBackgroundColor: "transparent"
									
								});
				
				var img = "/images/missingThumb.png";
				
				if ( articles.fieldByName("thumbnail_path") ){
					imgFile = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory+'../Library/Caches/', articles.fieldByName("thumbnail_path"));
					
					if ( imgFile.exists() ){
						img = imgFile;
					}
					
				}
				
				
				var itemThumb = Ti.UI.createImageView({
								image: img,
								left:	5,
								width: 	100,
								height:	100
							});
								
				var contentView = Ti.UI.createView({
					layout:	"vertical",
					left:	110,
					width:	205,
					height: 100,
					backgroundColor: "#012547",
					selectedBackgroundColor: "#012547"
				});
				
				var itemLabel = Ti.UI.createLabel({
								text:	articles.fieldByName("headline"),
								font:	{ fontSize:14,fontWeight:'bold' },
								color: "#ffffff",
								top: 	5,
								left:	5,
								width: 	170
							});
				
				contentView.add(itemLabel);			
				
				var itemDate = Ti.UI.createLabel({
								text:	articles.fieldByName("date"),
								font:	{ fontSize:10, fontStyle: "italic" },
								color:  "#ffffff",
								bottom:	20,
								left:	5,
								height:	20
							});
							
				if (articles.fieldByName("favorite") == 1 ) {
								
					var favTag = Ti.UI.createImageView({
									image:'/images/favTag.png',
									right:	0,
									top:	3,
									width: 	11,
									height:	20,
									zIndex: 99
								});
					
					row.add(favTag);
											
				} 
				
				contentView.add(itemDate);
				
				row.add(itemThumb);
				
				row.add(contentView);
								
				articlesData.push(row);	
				
				articles.next();
				
			}
			
			
		} else {
			
			// No articles exist, show message
			var row = Ti.UI.createTableViewRow({						
						id: 			articles.fieldByName("id"),
						rowHeight: 		'auto',
						height:			'auto',
						width:			'auto',
						hasChild: 		false,
						touchEnabled: 	false,
						top:			5,
						left:			5,
						right:			5,
						backgroundColor:'transparent',
						selectedBackgroundColor: "transparent"
					});
					
			var noFavImg = Ti.UI.createImageView({
									image:'/images/noFav.png',
									width: 	246,
									height:	172,
									top:	80
								});
							
			row.add(noFavImg);
							
			articlesData.push(row);	
			
		}
		
		
		
		article_table.setData(articlesData);
		
	}
	
	// ========================================
	// Event Listeners
	// ========================================
	/*
	self.addEventListener("open", function(){
		loadPage(false);
		
		if (AppTabGroup.tabBarVisible == true) {
	        AppTabGroup.animate({bottom:-50,duration:500});
	        AppTabGroup.tabBarVisible = false;
	    } 
	    
	});
	*/
	self.addEventListener("focus", function(){
			loadPage(true);
	});
	
	self.addEventListener("blur", function(){
		/*
		 if( self.children ){
		  while( self.children.length!=0  ) {
		    var len = self.children.length;
		    self.remove( self.children[len -1 ] );
		  }
		}
		*/
	});
	
	article_table.addEventListener("click", function(e){
			
			if ( e.rowData.url ) {
				var win = Titanium.UI.createWindow({
							url:				e.rowData.url,
							titleImage:			"../images/logo.png",
							backgroundImage:	'../images/background.png',
							
				});
				
				win.articleId = e.rowData.id;
				
			}
			
			Titanium.UI.currentTab.open(win,{animated:true});
		});
	
	self.addEventListener("close", function(){
		
		// Close down memory
		//db.close();
			
	});
