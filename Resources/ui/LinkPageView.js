
	// ========================================
	// Page Setup Variables
	// ========================================
	Ti.include("../common/GlobalVars.js");
	
	var self = Ti.UI.currentWindow;
	
	self.setTabBarHidden(true);
	
	self.orientationModes = [
		Titanium.UI.PORTRAIT,
		Titanium.UI.LANDSCAPE_LEFT,
		Titanium.UI.LANDSCAPE_RIGHT
	];
	
	var db = Ti.Database.open('KateDb');
	
	// ========================================
	// Page Controller Logic
	// ========================================
	function loadPage(rl){
		iPhoneView(self.linkUrl);
	}
	

	// ========================================
	// Page View
	// ========================================
	function iPhoneView(website){
		
		// Create WebView
		var webView = Titanium.UI.createWebView({
			url:website, 
			controls:true,
			width:	"100%",
			height:	"100%"	
		});
		self.add(webView);

	 
	    var loadIndBg = Ti.UI.createView({
	        width:40,
	        height:40,
	        backgroundColor:'#000000',
	        borderRadius:8,
	        opacity:0.5
	    });
	    webView.add(loadIndBg);       
	   
	    var actInd = Titanium.UI.createActivityIndicator({
			height:		50,
			width:		50,
			color:		"#333333",
			style:		Titanium.UI.iPhone.ActivityIndicatorStyle.PLAIN
		});
	    webView.add(actInd);
	    // create a back button image
	    var back_button = Ti.UI.createButton({
	       image: '/images/btnBack.png',
	       enabled:true,
	       width: 30,
	       height: 30
	    });
	    back_button.addEventListener('click', function(){
	        webView.goBack();
	    });
	    // create a forward button image
	    var forward_button = Ti.UI.createButton({
	       image: '/images/btnForward.png',
	       enabled:true,
	       width: 30,
	       height: 30
	    });
	    forward_button.addEventListener('click', function(){
	        webView.goForward();
	    });
	    
	     var refresh_button = Ti.UI.createButton({
	        systemButton: Titanium.UI.iPhone.SystemButton.REFRESH
	    });
	    refresh_button.addEventListener('click', function(){
	        webView.reload();
	    });
	    var flexSpace = Titanium.UI.createButton({
				systemButton:Titanium.UI.iPhone.SystemButton.FLEXIBLE_SPACE
			});
	    
	    // add the control buttons to the toolbar
	    self.toolbar = [back_button,flexSpace,refresh_button,flexSpace,forward_button];
	    // show activity when loading
	    
	    webView.addEventListener('beforeload',function(e){
	        loadIndBg.show();
	        actInd.show();
	    }); 
	    
	    webView.addEventListener('load',function(e){
	    	
	        loadIndBg.hide();
	        actInd.hide();

	        // set the control buttons
	        if(webView.canGoForward()){
	            forward_button.enabled = true;
	        } else {            
	            forward_button.enabled = false;
	        }
	        if(webView.canGoBack()){
	            back_button.enabled = true;
	        } else {            
	            back_button.enabled = false;
	        }
	        current_url = e.url;
	        
	        // After the page has loaded, count the view.  
	        // add a count for the page being viewing.  Just a count. No details
			var url = serverUrl + "/analytics/links/"+self.serverId+".json";
			var json;
			 
			var xhr = Ti.Network.createHTTPClient({});
			xhr.onerror = function() {};
			xhr.timeout = 2000;
	
			xhr.open("GET", url);
			xhr.send();
		    });
		
		
	}
	
	// ========================================
	// Event Listeners
	// ========================================
	self.addEventListener("open", function(){
		
		// load the page
		loadPage();
		//actInd.hide();
	
		
		/*
		if (Ti.App.customTabGroup.tabBarVisible == true) {
	        Ti.App.customTabGroup.animate({bottom:-50,duration:500});
	        Ti.App.customTabGroup.tabBarVisible = false;
	    } 
	    */
	    
	});
	
		
	
	self.addEventListener("close", function(){
		/*
		if (Ti.App.customTabGroup.tabBarVisible == false) {
	        Ti.App.customTabGroup.animate({bottom:50,duration:500});
	        Ti.App.customTabGroup.tabBarVisible = true;
	    }
		*/
		// Close down memory
		db.close();
			
	});
