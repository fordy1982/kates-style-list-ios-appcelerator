

	Ti.include("../common/GlobalVars.js");
	
	var self = Ti.UI.currentWindow;
	
	self.backgroundImage = "../images/background.png";
	
	var statusLabel = Ti.UI.createLabel({
			text:"Loading",
			height:"auto",
			color:"#000",
			textAlign:"center",
			font:{fontSize:16,fontWeight:"bold"}
		});
		
	self.add(statusLabel);