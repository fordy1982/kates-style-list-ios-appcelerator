function AppTabGroup() {
		
	//create module instance
	var self = Ti.UI.createTabGroup({
		//backgroundColor: "#012547"	
	});
	
	var articleListWindow = Titanium.UI.createWindow({
				url:		'ui/ArticleList.js',
				titleid:	'ui_articles',
				title:		"Home",
				titleImage:	"images/logo.png",
				backgroundImage:	'images/background.png',
				barColor:	"#012547"
			}); 
	
	var tab1 = Ti.UI.createTab({
		title: 	'Home',
		icon: 	'/images/tabHome.png',
		window: articleListWindow
	});
	
	articleListWindow.containingTab = tab1;
	
	var archiveListWindow = Titanium.UI.createWindow({
				url:'ui/ArchiveList.js',
				titleid:'ui_archive',
				title: "Archive",
				titleImage:	"images/logo.png",
				backgroundImage:	'images/background.png',
				barColor:	"#012547"
			}); 
			
	var tab2 = Ti.UI.createTab({
		title: 	"Archive",
		icon: 	'/images/tabArchive.png',
		window: archiveListWindow
	});
	
	archiveListWindow.containingTab = tab2;
	
	var favoritesListWindow = Titanium.UI.createWindow({
				url:'ui/FavoritesList.js',
				titleid:'ui_favorites',
				title: "Favourites",
				titleImage:	"images/logo.png",
				backgroundImage:	'images/background.png',
				barColor:	"#012547"
			}); 
			
	var tab3 = Ti.UI.createTab({
		title: 	"Favourites",
		icon: 	'/images/tabFav.png',
		window: favoritesListWindow
	});
	
	favoritesListWindow.containingTab = tab3;
	
	self.addTab(tab1);
	self.addTab(tab2);
	self.addTab(tab3);

	return self;
};

module.exports = AppTabGroup;
