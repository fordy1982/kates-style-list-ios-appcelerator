	// ========================================
	// Page Setup Variables
	// ========================================
	Ti.include("../common/GlobalVars.js");
	
	var self = Ti.UI.currentWindow;
	self.backgroundImage = "../images/background.png";
	
	var db;
	var swipeHand;
	var fadeoutAnimation;
	var shownHand = 0;
	var lastTimestamp;
	var updating = false;
	
	// Create a table view
		var article_table = Ti.UI.createTableView({
					backgroundColor:'transparent',
					top:	5,
					separatorStyle: Titanium.UI.iPhone.TableViewSeparatorStyle.NONE
					
				});	 
		
	self.add(article_table);
	//self.add(ad);
	

	// ========================================
	// Page Controller Logic
	// ========================================
	function loadPage(rl){
		
		// Look up the categories
		
		categories = db.execute("SELECT c.* FROM categories c, article_categories ac WHERE c.server_id = ac.category_server_id GROUP BY c.server_id");
		
		if (isIPhone || isIPad){
			iPhoneView(categories);
		}
		
	}
	
	
	// ========================================
	// Page View
	// ========================================
	function iPhoneView(data){
	
		var categoriesData = [];
		
		if ( data.rowCount > 0 ) {
		
			// Loop thru the categories
			while ( data.isValidRow() ) {
				
				// Set a section title for the archive category
				var section = Titanium.UI.createTableViewSection();
				section.headerTitle = data.fieldByName("name");
				
				// Set a variable to base the left property for the Image View
				// TODO: Check this is needed later when refactoring
				var viewLeft = 0;
				
				// Create a row and put the scrollable view in it
				var headerRow = Ti.UI.createTableViewRow({						
								backgroundColor:	"transparent",
								height:				30,
								hasChild: 			false,
								id: 				data.fieldByName("id") + "_header",
								left:				5,
								rowHeight: 			30,
								selectedBackgroundColor: "#00395d",
								touchEnabled: 		false,
								top:				5,
								width:				310
							});
				// selectedBackgroundColor: "#012547",
				
				var rowHeaderView = Ti.UI.createView({
								backgroundColor:	"#012547",
								left:	5,
								right: 	5,
								top:	5,
								height:	30
				});
					
				// Create a lable to hold the Category title in
				var rowHeaderLabel = Ti.UI.createLabel({
								text:			data.fieldByName("name"),
								font:	{ fontSize:14,fontWeight:'bold' },
								color: "#ffffff",
								top: 	5,
								left:	5
				});
				
				// Image to go on right hand side of the header row
				var rowHeaderArrow = Ti.UI.createImageView({
								height:	14,
								image: 	"/images/arrow.png",
								right:	5,
								top:	8,
								width: 	8
				});
				
				// Add contents to row header
				rowHeaderView.add(rowHeaderLabel);
				rowHeaderView.add(rowHeaderArrow);
				
				// Add label to the row
				headerRow.add(rowHeaderView);
				
				// Create a row and put the scrollable view in it
				var row = Ti.UI.createTableViewRow({						
								height:			110,
								hasChild: 		false,
								id: 			data.fieldByName("id"),
								left:			5,
								rowHeight: 		110,
								selectedBackgroundColor: "transparent",
								touchEnabled: 	false,
								top:			5,
								width:			310
							});
				
				// Call for content
				var scrollContent = getArticles(null, data.fieldByName("server_id"), false);
				
				// Create Scrollable view
				var scroller = Ti.UI.createScrollView({
					  contentHeight:	100,
					  contentWidth:		105*scrollContent.length,
					  width:			310,
					  height:			105,
					  scrollType: 		'horizontal',
					  name:				'scrollView',
					  verticalBounce: 	false,
					  horizontalBounce: true,
					  top:				5
					});
				
				// Loop through the articles and place on the row
				for ( var i=0; i<scrollContent.length; i++ ) {
					
					// Set the items left property
					scrollContent[i].left = i * 105;
					
					// Add content to the scroller
					scroller.add(scrollContent[i]);	
					
				}
				
				var w = 105*scrollContent.length;
				
				// Add attributes to the table for event listener
				row.lastTimestamp 	= lastTimestamp;
				row.category		= data.fieldByName("server_id");
								
				// Add the scroller to the table row
				row.add(scroller);
				
				// Add row to section
				//section.add(row);
							
				// Add section to the array being used to store all table data	
				categoriesData.push(headerRow);
				categoriesData.push(row);		
				
				// Loop to next category row
				data.next();		
				
			}
			 
			/// HERE
			db.close(); 
			 
			
		} else {
			// No Data
			var row = Ti.UI.createTableViewRow({						
						id: 			data.fieldByName("server_id"),
						rowHeight: 		100,
						height:			100,
						hasChild: 		false,
						touchEnabled: 	false,
						top:			5,
						left:			5,
						right:			5,
						backgroundColor:'transparent',
						selectedBackgroundColor: "transparent"
					});
					
			var itemLabel = Ti.UI.createLabel({
								text:	"No Results...",
								font:		{ fontSize:16,fontWeight:'bold' },
								left:	10,
								width: 	300,
								height:	30	
							});
							
			row.add(itemLabel);
					
			categoriesData.push(row);
			
		}
		
		// Add the data to the table
		article_table.setData(categoriesData);

		if ( shownHand == 0 ) {
			swipeHand = Ti.UI.createImageView({
				image: "/images/swipe.png",
				width:	75,
				height:	75,
				top:	50,
				zIndex:	99
			});
			
			self.add(swipeHand);
			
			
			fadeoutAnimation = Ti.UI.createAnimation({
			    curve: Ti.UI.iOS.ANIMATION_CURVE_EASE_OUT,
			    opacity: 0, 
			    duration: 3500
			});
		
		}
		
	}
	
	function getDbArticles(timestamp, category){
		if ( timestamp != null){
			lastTs = " AND a.timestamp < " + timestamp;
		}else{
			lastTs = "";
		}
		
		// Search for the categories articles and then loop them in a scrollable view
		return db.execute("SELECT a.id, a.favorite, a.date, a.date_created, a.thumbnail_path, a.timestamp FROM article_categories ac, articles a WHERE a.server_id = ac.article_server_id AND ac.category_server_id  = ? "+lastTs+" GROUP BY a.id ORDER BY a.timestamp DESC LIMIT 9", category);
		
	}

	// Function to get articles and return an array of views
	function getArticles(lastTs, category, silent){
		
		// Init empty array to put article / images in ready to return 
		aryViews = [];
		
		// Look up articles
		var articles = getDbArticles(lastTs, category);
		
		if ( articles != null && articles.rowCount > 0 ) {
			
			while ( articles.isValidRow() ) {
				
				// Create view container for image and favtag
				var contentView = Ti.UI.createView({
						id:		articles.fieldByName("id"),
						height: 100,
						top:	5,
						width:	100
					});
				
				var img = "/images/missingThumb.png";
				
				if ( articles.fieldByName("thumbnail_path") ){
					
					// Get image
					imgFile = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory+'../Library/Caches/', articles.fieldByName("thumbnail_path"));
					
					if ( imgFile.exists() ){
						img = imgFile;
					}
					
				}
				
				var itemThumb = Ti.UI.createImageView({
								image: img,
								left:	0,
								width: 	100,
								height:	100,
								id: 	articles.fieldByName("id"),
								name:	"image"
							});
					
				// Add the image to the view
				contentView.add(itemThumb);
				
				
				// Check to see if the article is a favorite
				// If so, add the tag
				if (articles.fieldByName("favorite") == 1 ) {
					
					// Create favTag View		
					var favTag = Ti.UI.createImageView({
									image:'/images/favTag.png',
									right:	5,
									top:	0,
									width: 	11,
									height:	20,
									zIndex: 99
								});
					
					// Add it to the container view
					contentView.add(favTag);
											
				} 
				
				
				
				// Add the image and fav tag to the contentView array
				aryViews.push(contentView);
				
				lastTimestamp = articles.fieldByName("timestamp");
				
				// Loop to next article
				articles.next();
					
			}
			
		} else {
			// No records
			
			if (!silent){
				// TODO: Show no records 
			}
			
			
		}
		
		return aryViews;
		
	}
	

	// ========================================
	// Event Listeners
	// ========================================
	
	var scrollActionable=false;
	
	self.addEventListener("focus", function(){
		db = Ti.Database.open('KateDb');
		loadPage();
	});
	
	self.addEventListener("blur", function(){
		if (db){
			db.close();
		}
		
	});
	
	function checkScroll(e){
		
		var offset = e.source.contentOffset.x;
		var width = e.source.size.width;
		var total = offset + width;
		var theEnd = e.source.contentWidth;
		var distance = theEnd - total;
		
		if ( scrollActionable == true && updating == false && distance < 50 ) {
			
			updating = true;
			scrollActionable = false;
			
			// Set global var so the sync script knows where to call back to
			Ti.App.syncBack 		= "updateCategoryArticles";
			Ti.App.rowIndex 		= e.index;
			Ti.App.ts				= e.row.lastTimestamp;
			Ti.App.categoryToUpdate = e.row.category;
			Ti.App.scrollerToUpdate = e.source;
			Ti.App.rowToUpdate 		= e.row;
			
			Ti.App.addEventListener("updateCategoryArticles", updateRow);
			
			// Do sync
			Ti.App.fireEvent('sync_category_from', {cat: Ti.App.categoryToUpdate, ts: Ti.App.ts});
			
		}
		
	} 
	
	article_table.addEventListener("scroll", function(e){ checkScroll(e); });
	
	article_table.addEventListener("dragStart", function(e){ 
			scrollActionable=true;
	});
	
	
	var updateRow = function(e){
		
		Ti.App.removeEventListener("updateCategoryArticles", updateRow);
		
		updating = false;
		
		db = Ti.Database.open('KateDb');
		
		var articlesToAppend = getArticles(Ti.App.ts, Ti.App.categoryToUpdate);
		
		var scroller = Ti.App.scrollerToUpdate;
		var row = Ti.App.rowToUpdate;
		var updateLastTimestamp;
		// Loop through the articles and place on the row
		for ( var i=0; i<articlesToAppend.length; i++ ) {
			
			// Set the items left property
			articlesToAppend[i].left = scroller.contentWidth+(i * 105);
			
			
			// Add content to the scroller
			scroller.add(articlesToAppend[i]);	
			
		}
		
		// Add attributes to the table for event listener
		row.lastTimestamp 		= lastTimestamp;
		scroller.setContentWidth(scroller.contentWidth+(articlesToAppend.length * 105));

		db.close();		
		
	};
	
	
	
	article_table.addEventListener("click", function(e){
		
		if (e.source.name == 'image') {
            
            var win = Titanium.UI.createWindow({
				url:				'/ui/ArticlePage.js',
				titleImage:			"../images/logo.png",
				backgroundImage:	'../images/background.png'
			});
			
			win.articleId = e.source.id;
        	
        	Titanium.UI.currentTab.open(win,{animated:true});
        	    
        }
        
		
		
	});



	self.addEventListener("close", function(){
		
		// Close down memory
		//categories.close();
		db.close();
			
	});


	var removeHand = function(){
		
		shownHand = 1;
		
		if (swipeHand){
			swipeHand.animate(fadeoutAnimation);
		}
		self.removeEventListener("postlayout", removeHand);
	};
	
	self.addEventListener("postlayout", function(){
		removeHand();
	});


