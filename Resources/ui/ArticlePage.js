
	// ========================================
	// Page Setup Variables
	// ========================================
	Ti.include("../common/GlobalVars.js");
	
	var self = Ti.UI.currentWindow;
	self.barColor = "#012547";
	
	var db = Ti.Database.open('KateDb');
	
	var swipeHand;
	var fadeoutAnimation;
	var btnRight;
	
	// ========================================
	// Page Controller Logic
	// ========================================
	function loadPage(rl){
		
		// Load article
		var aSql = db.execute("SELECT * FROM articles WHERE id = ? LIMIT 1", self.articleId);
		
		var dbArticle = [];
		
		if ( aSql.rowCount > 0 ){ 
			while ( aSql.isValidRow() ) {
				
				dbArticle.push({
					id:	aSql.fieldByName("id"),
					headline:	aSql.fieldByName("headline"),
					server_id:	aSql.fieldByName("server_id"),
					intro:	aSql.fieldByName("intro"),
					body:	aSql.fieldByName("body"),
					featured:	aSql.fieldByName("featured"),
					date:	aSql.fieldByName("date"),
					favorite:	aSql.fieldByName("favorite"),
					date_created:	aSql.fieldByName("date_created")
				});
				
				imagesSql = db.execute("SELECT * FROM images WHERE article_server_id = ? ORDER BY position ASC", aSql.fieldByName("server_id"));

				
				var linksSql = db.execute("SELECT * FROM links WHERE article_server_id = ?", aSql.fieldByName("server_id"));
				
				var catSql = db.execute("SELECT * FROM article_categories, categories WHERE article_categories.category_server_id = categories.server_id AND article_server_id =?", aSql.fieldByName("server_id") );
				
				aSql.next();
			}
			
			iPhoneView(dbArticle, imagesSql, catSql, linksSql);
		}
		
		
	}
	
	
	
	// ========================================
	// Page View
	// ========================================
	function iPhoneView(article, images, categories, links){
		
		/*
		 * 1) Setup images & add to a scrollable view 
		 * 2) Create Headline
		 * 3) Add date & body copy
		 * 4) Show links
		 * 5) Show categories
		 * 6) Show Add to Favorites button
		 *  
		 */
		
		
		// Create ImageViews for the Scrollable from the images in the db
		
		// Init empty array for the images to be 
		// added to the scrollable
		var dbImages = [];
		var tallestImg = 0;
		var imageCount = images.rowCount;
		
		// Check for records
		if ( imageCount > 0 ) {
			
			// Loop through the image records
			while ( images.isValidRow() ) {
				
				// Get file
				var img = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory+'../Library/Caches/', images.fieldByName("path_article") );
				
				// Get an error if the image isn't found so check before adding to the UI
				if ( img != null ) {
					
					var imgView = Titanium.UI.createImageView({ 
							image: img,
							top: 0 
					});
					
					// Push the image onto the array
					dbImages.push(imgView);
					Ti.API.log("Image: " + images.fieldByName("server_id"));
					if ( imgView.toImage().height > tallestImg ) {
						tallestImg = imgView.toImage().height;
					}
				
				}
				
				// Loop to next image record
				images.next();
			}
			
		} else {
			
			var imgView = Titanium.UI.createImageView({ 
							image: "/images/missingMedium.png",
							width:	310,
							height:	310
					});
					
			dbImages.push(imgView);
			tallestImg = 320;
			
		}
		
		
		// Create Scrollable view to hold the images in
		var imageScroll = Ti.UI.createScrollableView({
		    contentHeight: tallestImg,
		    currentPage:	0,
		    height:			tallestImg,
		    top: 			5,
		    width:			310,
		    views:			dbImages
		}); //310
		
		
			
		// Create headline 
		var headline = Ti.UI.createLabel({
			color:  "#818181",
			height: 'auto',
			font:	{ fontSize:14, fontWeight: "bold" },
			layout: 'vertical',
			text:	article[0]["headline"],
			width: 300,
			top: 10
		});	
		
		// Create date 
		var aDate = Ti.UI.createLabel({
			text:	article[0]["date"],
			font:	{ fontSize:12, fontStyle: "italic" },
			color:  "#818181",
			layout: 'vertical',
			top: 	headline.top + headline.contentHeight + 5,
			width: 300,
			top: 0,
			height: 'auto'
		});	
		
		
		// Create the body copy
		var body = Ti.UI.createLabel({
			text:	article[0]["body"].replace(/\\n/g, "\n"),
			font:	{ fontSize:14 },
			color:  "#818181",
			width: 300,
			top: 5 ,
			height: 'auto'
		});	
		
		
		
		var contHeight = imageScroll.toImage().height + headline.toImage().height + aDate.toImage().height + (body.toImage().height * 2) + 50;

		
		// Create view container to hold all the content above the links row
		// Images, {headline, date, body} 
		var topView = Ti.UI.createView({
			children: [imageScroll, headline, aDate, body],
			height: contHeight,
			layout: 'vertical',
			width: 'auto'
		}); //height:	600,
		
		
		// Add a hand/swipe icon if there are more than 1 image
		
		if ( imageCount > 1 ){
			
			swipeHand = Ti.UI.createImageView({
				image: "/images/swipe.png",
				width:	75,
				height:	75,
				top:	90,
				zIndex:	99
			});
			
			self.add(swipeHand);
			
			fadeoutAnimation = Ti.UI.createAnimation({
			    curve: Ti.UI.ANIMATION_CURVE_EASE_OUT,
			    opacity: 0, 
			    duration: 5000
			});
		}
		
		
		var linkRows = [];
		
		// Create the table rows for the links to sit in
		if ( links.rowCount > 0 ) {
			
			while ( links.isValidRow() ) {
				
				var row = Ti.UI.createTableViewRow({
					id: 			links.fieldByName("id"),
					linkUrl:		links.fieldByName("url"),
					serverId:		links.fieldByName("server_id"),
					height: 		53,
					width:			300,
					left:			10,
					layout:			"vertical",
					backgroundImage: "/images/linkRow.png",
					backgroundRepeat: true,
					hasChild: 		true,
					touchEnabled: 	true,
					rightImage:		"../images/arrow.png",
				});
				
				var getLookLabel = Ti.UI.createLabel({
					text:		"GET THE LOOK:",
					font:		{ fontSize:14,fontWeight:'bold' },
					color:		'white',
					width:		'auto',
					textAlign:	'left',
					top:		10,
					left:		13,
					height:		16
				});
				var linkLabel = Ti.UI.createLabel({
					text:		links.fieldByName("link_label"),
					font:		{ fontSize:14,fontWeight:'bold' },
					color:		'white',
					width:		'auto',
					textAlign:	'left',
					top:		5,
					left:		13,
					height:		16
				});
				
				row.add(getLookLabel);
				row.add(linkLabel);
				linkRows.push(row);
				
				links.next();
				
			}
			
		} else {
			
		}
		
		
		// Footer view
		var footerView = Ti.UI.createView({
			top: 		0,
			backgroundImage:	"/images/articleFooter.png",
			width: 		'auto',
			height:		125
		});
		
		
		var btnLabel;
		/*
		var btnFav  = Ti.UI.createButton({
			backgroundImage:"../images/favTagAdd.png",
			style: Ti.UI.iPhone.SystemButtonStyle.PLAIN,
			borderRadius : 0,
			width: 	304,
			height: 53,
			top:	30
		});
		
		footerView.add(btnFav);
		*/
		
		var btnFavAdd = Ti.UI.createButton({
			image:"../images/favtagAdd.png",
			backgroundImage:"../images/favTagAdd.png",
			borderRadius : 0,
			width: 	304,
			height: 53,
			top:	30
		});
		
		var btnFavMinus = Ti.UI.createButton({
			image:"../images/favtagMinus.png",
			backgroundImage:"../images/favTagMinus.png",
			borderRadius : 0,
			width: 	304,
			height: 53,
			top:	30
		});
		
		if (article[0]["favorite"] == 1 ) {
			footerView.add(btnFavMinus);
		} else {
			footerView.add(btnFavAdd);
		}
		
		btnFavMinus.addEventListener("click", function(){
				// Article is featured so remove
				db.execute("UPDATE articles SET favorite = 0 WHERE id = ?", article[0]["id"]);

				article[0]["favorite"] = 0;
				footerView.remove(btnFavMinus);
				footerView.add(btnFavAdd);
		});				
		
		btnFavAdd.addEventListener("click", function(){
		
				// Add article to Favorites
				db.execute("UPDATE articles SET favorite = 1 WHERE id = ?", article[0]["id"]);
				
				article[0]["favorite"] = 1;
				
				footerView.remove(btnFavAdd);
				
				footerView.add(btnFavMinus);
		
		});
		
		
		if ( categories.rowCount > 0 ) {
			// Add Categories
			var catLabel = Ti.UI.createLabel({
						text:		"Article listed in: ",
						font:		{ fontSize:14,fontWeight:'bold', fontStyle: "italic" },
						color:		"#818181",
						width:		'auto',
						textAlign:	'left',
						top:		93,
						left:		10,
						height:		16
					});
			
			footerView.add(catLabel);
			
			var catString = "";
			var catCount = 1;
			
			while ( categories.isValidRow() )
			{
				
				catString += categories.fieldByName("name");
				
				if ( catCount < categories.rowCount ) 
				{

					catString += ", ";
					
				} 
				
				catCount++;
				
				categories.next();
				
			}
			
			var catLabelContent = Ti.UI.createLabel({
					text:		catString,
					font:		{ fontSize:14, fontStyle: "italic" },
					color:		"#818181",
					width:		'auto',
					textAlign:	'left',
					top:		93,
					left:		120,
					height:		'auto'
			});
			
			footerView.add(catLabelContent);	
			
		}
		
		// Create a table view to add everything to.
		var tableView = Ti.UI.createTableView({
			data: 				linkRows,
			backgroundColor: 	'transparent',
			separatorStyle: Titanium.UI.iPhone.TableViewSeparatorStyle.NONE,
			headerView: 		topView,
			footerView:			footerView,
			width:				320
		});
		
		tableView.addEventListener("click", function(e)
		{
			
			var win = Titanium.UI.createWindow({
				url:				"/ui/LinkPageView.js",
				title:				"Link Website",
				titleImage:			"/images/logo.png",
				backgroundImage:	'/images/background.png',
				barColor:			"#012547"
			});
			
			win.linkId = e.rowData.id;
			win.linkUrl = e.rowData.linkUrl;
			win.serverId = e.rowData.serverId;
			
			Titanium.UI.currentTab.open(win,{animated:true});
			
		});
		
		self.add(tableView);		
		
	}
	
	var hideSwipeIcon = function()
	{
		
		swipeHand.hide();
		
		self.removeEventListener("touchstart",hideSwipeIcon);
		
	};
	
	
	
	// ========================================
	// Event Listeners
	// ========================================
	self.addEventListener("open", function()
	{	
		// load the page
		loadPage();
		
		// add a count for the page being viewing.  Just a count. No details
		var url = serverUrl + "/analytics/articles/"+self.articleServerId+".json";
		var json;
		 
		var xhr = Ti.Network.createHTTPClient({});
		xhr.onerror = function() {};
		xhr.timeout = 2000;

		xhr.open("GET", url);
		xhr.send();
		
	});
	
	var removeHand = function()
	{	
		// Add the right nav bar button
		//Ti.UI.currentWindow.rightNavButton = btnRight;
		
		if (swipeHand){
			swipeHand.animate(fadeoutAnimation);
		}
		
	};
	/*
	self.addEventListener("touchstart", function()
	{
		// Check for swipeHand
		
	});
	*/
	
	self.addEventListener("focus", function()
	{
		removeHand();
		
		if (AppTabGroup.tabBarVisible == false) 
		{
	        AppTabGroup.animate({bottom:0,duration:500});
	        AppTabGroup.tabBarVisible = true;
	    }
	});
	
	self.addEventListener("close", function()
	{
		
		// Close down memory
		db.close();
			
	});