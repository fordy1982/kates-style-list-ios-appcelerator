


	// ========================================
	// Page Setup Variables
	// ========================================
	Ti.include("../common/GlobalVars.js");
	
	var strToday = todayGenerator();
	var fileQueue = [];
	var thumbQueue = [];
	var calledComplete = false;
	
	// ========================================
	// Get latest data
	// ========================================
	
	var syncCategories = function(){
		
		Ti.API.log("Starting syncCategories");
		
		/*
		 * Check for cleanse property
		 * If found, skip
		 * else do a clean of all records except those that are favourited
		 * 
		 */

		if(!Titanium.App.Properties.hasProperty('prClean')){
			//do clean articles
			Ti.API.log("In prClean");
			var dbClean = Ti.Database.open('KateDb');
			dbClean.execute('DELETE FROM articles WHERE favorite ISNULL');
			dbClean.close();
			Ti.App.Properties.setBool('prClean', true);
		}else{
			Ti.API.log("prClean exists, skipping");
		}
		
		var url = serverUrl + "/categories.json";
		var json;
		
		var xhr = Ti.Network.createHTTPClient({});
			
			xhr.enableKeepAlive = false; 
			
			xhr.onload = function() {
				
				json = JSON.parse(this.responseText);
				
				var db2 = Ti.Database.open('KateDb');
				var existing_ids = db2.execute("SELECT id, date_created FROM categories");
				
				var local_categories = {};
				
				while ( existing_ids.isValidRow() ) {
					
					local_categories[existing_ids.fieldByName("id")] = {};
					local_categories[existing_ids.fieldByName("id")]["date_created"] = existing_ids.fieldByName("date_created"); 
					existing_ids.next();
				}
				existing_ids.close();
				
					// check for JSON content
				if ( json != null ) {
					
					for ( i=0; i < json.length; i++ ) {
					
						var category = json[i].category;
						
						
						// Check if article, exists
						if ( _.has(local_categories, category.id) ) {
						
							// If it does, check date inserted on the app to see if it needs updating
							
							// Setup local & server dates to compare 
		        			var serverDT 	= dp(category.updated_at); 
		        			var localDT 	= dp(local_categories[category.id]['date_created']);
							
							// if server version is newer than the local version, update
		        			if ( serverDT > localDT ){
		        				db2.execute("UPDATE categories SET name = ?, date_created = ? WHERE server_id = ?", category.name, todayGenerator(), category.id);
		        			}else{
		        				// Do nothing
		        			}
							
						} else {
							// If it does not, insert
							db2.execute("INSERT INTO categories (name, date_created, server_id) VALUES (?,?,?)", category.name, todayGenerator(), category.id);
								
						}		
						
					}
					

					db2.close();
					Ti.API.fireEvent("full_sync");
					// Ti.API.removeEventListener("sync_categories");
					
				}
				
			
			};
		
		xhr.onerror = function(e) {
			alert("Error Downloading Articles - Please try again and contact Stylelister if the issue continues");
			
			Ti.App.fireEvent("sync_complete");
		};			
		
		xhr.timeout = 3000;
		
		xhr.open("GET", url);
		xhr.send();
		
		
	};
	
	
	var fullSync = function(){
		
		Ti.API.log("Starting fullSync");
		
		// Clear Event Listener so it doesn't get called multiple times again
		//Ti.API.removeEventListener("full_sync", fullSync);	
		
		var url = serverUrl + "/articles.json";
		var json;
		 
		var xhr = Ti.Network.createHTTPClient({});
		
		xhr.enableKeepAlive = false;
		
		xhr.onload = function() {	
				var res = syncArticles(JSON.parse(this.responseText));
		};
		
		xhr.onerror = function() {
			alert("Error Downloading Articles");
			Ti.App.fireEvent("sync_complete");
		};			
		
		xhr.timeout = 3000;

		xhr.open("GET", url);
		xhr.send();
		
	};
	
	// request sync from timestamp
	var syncFrom = function(ts){
		
		Ti.App.removeEventListener("sync_from", syncFrom);
		
		var url = serverUrl + "/articles/before/"+ts+".json";
		//Ti.API.trace(url);
		var json;
		 
		var xhr = Ti.Network.createHTTPClient({});
		
		xhr.enableKeepAlive = false;
		
		xhr.onload = function() {
				Ti.API.log(this.responseText);
				var res = syncArticles(JSON.parse(this.responseText));
								
		};
		
		xhr.onerror = function() {
			alert("Error Downloading Articles");
			Ti.App.fireEvent("sync_complete");
		};		
		
		xhr.timeout = 3000;
		
		xhr.open("GET", url);
		xhr.send();
		
	};
	
	
		// request sync from timestamp
	var syncCategoryFrom = function(category,ts){
		
		Ti.App.removeEventListener("sync_category_from", syncCategoryFrom);
		
		var url = serverUrl + "/articles/by_category/"+category+"/"+ts+".json";
		var json;
		 
		var xhr = Ti.Network.createHTTPClient({});
		
		xhr.enableKeepAlive = false;
		
		xhr.onload = function() {
				if ( this.responseText != "[]" ) {
					var res = syncArticles(JSON.parse(this.responseText));
				} else {
					Ti.App.fireEvent("sync_complete");
				}
								
			};
		
		xhr.onerror = function() {
			alert("Error Downloading Articles");
			Ti.App.fireEvent("sync_complete");
		};			
		
		xhr.timeout = 3000;
		
		xhr.open("GET", url);
		xhr.send();
		
	};
	
	
	var logSync = function(){
		
		Ti.API.removeEventListener("log_sync", logSync);
		
		var db2 = Ti.Database.open('KateDb');
		db2.execute("INSERT INTO sync_logs (timestamp) VALUES (?)", todayGenerator());
		db2.close();
		
		
	};
	
	
	function syncArticles(json){
		
		var db2 = Ti.Database.open('KateDb');
		
		var existing_ids = db2.execute("SELECT id, server_id, date_created FROM articles");
				
		var local_articles = {};
		
		// Loop through the loaded articles and add to an array
		while (existing_ids.isValidRow()) {
			local_articles[existing_ids.fieldByName("server_id")] = {};
			local_articles[existing_ids.fieldByName("server_id")]['local_id'] = existing_ids.fieldByName("id");
			local_articles[existing_ids.fieldByName("server_id")]['date_created'] = existing_ids.fieldByName("date_created");
			existing_ids.next();
		}
		
		existing_ids.close();
		db2.close();
		
		
		// check for JSON content
		if ( json != null ) {
			
			// through in begin statement 
			//db2.execute("BEGIN");
			
			for ( i=0; i < json.length; i++ ) {
				
				//var db2 = Ti.Database.open('KateDb');
				var article = json[i].article;
				var thumbnailPath;
				var newThumbnail = "";
				
				Ti.API.log("ARTICLE: " + article.headline);
				
				// Check whether the article needs deleting to start with
				if ( article.deleted_at != null || article.site != siteCode ){
					
					var db2 = Ti.Database.open('KateDb');
					db2.execute("DELETE FROM articles WHERE server_id = ?", article.id);
					db2.close();
				} else {
				
					// Check if article, exists
					if ( _.has(local_articles, article.id) ) {
					
						// If it does, check date inserted on the app to see if it needs updating
						
						// Setup local & server dates to compare 
	        			var serverDT 	= dp(article.updated_at); 
	        			var localDT 	= dp(local_articles[article.id]['date_created']);
						
						// if server version is newer than the local version, update
	        			if ( serverDT > localDT ){
	        				
	        				if(article.thumbnail_content_type != null){
	        					
	        					var fileType;
	    						
	    						
	    						
			        			// Get the image's file type
			        			switch(article.thumbnail_content_type){
			        				case "image/jpeg":
			        					fileType = "jpg";
			        					break;
			        				case "image/png":
			        					fileType = "png";
			        					break;
			        				case "image/gif":
			        					fileType = "gif";
			        					break;	
			        			}
			        			
			        			// Set the inbound image's file name to the asset ID + Filetype
			        			newThumbnail = article.id + "_thumbnail" + "." + fileType;
			        			// Add the image to the image to the download queue
			        			thumbQueue.push([article.thumbnail, newThumbnail]);
			        			
	        					
	        				}else{
	        					article.thumbnail = "";
	        				}
	        				var db2 = Ti.Database.open('KateDb');
	        				db2.execute("UPDATE articles SET headline = ?, intro = ?, body = ?, date = ?, timestamp=?, featured = ?, date_created = ?, thumbnail_path=? WHERE server_id = ?", article.headline, article.intro, article.body, article.date, article.timestamp, article.featured, article.updated_at, newThumbnail, article.id);
	        				db2.close();
	        			}
						
					} else if (article.site == siteCode) {
						// If it does not, insert
						
						
						if(article.thumbnail_content_type != null){
	        					
	        					var fileType;
	    			
			        			// Get the image's file type
			        			switch(article.thumbnail_content_type){
			        				case "image/jpeg":
			        					fileType = "jpg";
			        					break;
			        				case "image/png":
			        					fileType = "png";
			        					break;
			        				case "image/gif":
			        					fileType = "gif";
			        					break;	
			        			}
			        			
			        			// Set the inbound image's file name to the asset ID + Filetype
			        			newThumbnail = article.id + "_thumbnail" + "." + fileType;
			        			// Add the image to the image to the download queue
			        			thumbQueue.push([article.thumbnail, newThumbnail]);
	        					
	        				}else{
	        					article.thumbnail = "";
	        				}
	        				
	        				
						var db2 = Ti.Database.open('KateDb');
						db2.execute("INSERT INTO articles (headline, intro, body, date, timestamp, featured, date_created, server_id, thumbnail_path) VALUES (?,?,?,?,?,?,?,?,?)", article.headline, article.intro, article.body, article.date, article.timestamp, article.featured, article.updated_at, article.id, newThumbnail);
						db2.close();
					}	
					
				}	
				
				
				db2.close();
				
				//======================
        		// Get Item's Image Assets
        		//======================
    			
    			if ( article.assets != null){
    			
        			// Setup variables
	        		var images	= article.assets;
	        		
	        		if ( images.length > 0 ) {
		        		syncImages(images, article);
		        	} 
    			
    			} 
				
				
				//======================
        		// Get Item's Links
        		//======================
    			
    			if ( article.links != null){
    			
        			// Setup variables
	        		var links	= article.links;
	        		
	        		if ( links.length > 0 ) {
		        		syncArticleLinks(links, article);	
		        	} 
    			
    			} 
				
				
				//======================
        		// Get Item's Categories
        		//======================
    			
    			if ( article.categories != null){
    			
        			// Setup variables
	        		var categories	= article.categories;
	        		
	        		if ( categories.length > 0 ) {	
		        		syncArticleCategories(categories, article);
		        	} 
    			
    			} 
    			
    			
				
			} // End of articles loop
			
		}
		
		// log the sync has happened
		Ti.API.fireEvent("log_sync");
			
		if ( thumbQueue.length > 0 || fileQueue.length > 0 ) {
			downloadFiles();
		} else {
			Ti.App.fireEvent("sync_complete");
		}
		
		return true;
		
	}
	
	
	function syncImages(images, article){
		
		var db2 = Ti.Database.open('KateDb');
		
		var localImages	= {};
				        		
		// Search for existing Agreements
		var checkImages = db2.execute("SELECT server_id, date_created FROM images WHERE article_server_id = ?", article.id);
		
		
		// loop through the existing inventories if they exist
    	while (checkImages.isValidRow()) {
    		localImages[checkImages.fieldByName("server_id")] = {};
    		localImages[checkImages.fieldByName("server_id")]['date_created'] = checkImages.fieldByName("date_created");
    		checkImages.next();
    	}
    	
    	checkImages.close();
		
		// Loop through inbound data
		for (var image_i=0; image_i < images.length; image_i++){
			
			var image = images[image_i].asset;
			
			if ( image.deleted_at != null ){	
					db2.execute("DELETE FROM images WHERE server_id = ?", image.id);
			} else {
				Ti.API.log("ASSET: " + image.id);
				// Check to see if the current inbound inventory exists in the local DB already
	        	if ( _.has(localImages, image.id) ) {
	   				Ti.API.log("is on app already");
	   	        			// Setup local & server dates to compare 
	    			var serverDT 	= dp(image.image_updated_at); 
	    			var localDT 	= dp(localImages[image.id]['date_created']); 
	    			
	    			// if server version is newer than the local version, update
	    			if ( serverDT > localDT ){
	   				 	Ti.API.log("server newer");	
	    				var fileType;
	    			
	        			// Get the image's file type
	        			switch(image.item_content_type){
	        				case "image/jpeg":
	        					fileType = "jpg";
	        					break;
	        				case "image/png":
	        					fileType = "png";
	        					break;
	        				case "image/gif":
	        					fileType = "gif";
	        					break;	
	        			}
	        			
	        			// Set the inbound image's file name to the asset ID + Filetype
	        			var newMediumImageName = image.id + "_medium" + "." + fileType;
	        			var newThumbImageName; // = image.id + "_thumb" + "." + fileType;
	        			// Add the image to the image to the download queue
	        			fileQueue.push([image.path_medium, newMediumImageName]);
	        			//fileQueue.push([image.path_thumb, newThumbImageName]);
	    				
	    				db2.execute("UPDATE images SET path_thumb=?, path_article=?, article_server_id=?, position=?, label=?, date_created=? WHERE server_id=?", newThumbImageName, newMediumImageName, article.id, image.position, image.label, strToday, image.id );
	    			
	    			}else{
	    				// Do nothing
	    				Ti.API.log("app newer");
	    			}
	    			
				} else {
					Ti.API.log("not yet on app");
	    			// Inventory not local so can be inserted into system
	    			
	    			var fileType;
	    			
	    			// Get the image's file type
	    			switch(image.image_content_type){
	    				case "image/jpeg":
	    					fileType = "jpg";
	    					break;
	    				case "image/png":
	    					fileType = "png";
	    					break;
	    				case "image/gif":
	    					fileType = "gif";
	    					break;
	    					
	    			}
	    			
	    			// Set the inbound image's file name to the asset ID + Filetype
	    			var newMediumImageName = image.id + "_medium" + "." + fileType;
	    			var newThumbImageName;// = image.id + "_thumb" + "." + fileType;
	    			
	    			// Add the image to the image to the download queue
	    			fileQueue.push([image.path_medium, newMediumImageName]);
	        		//fileQueue.push([image.path_thumb, newThumbImageName]);
					
					// SQL to add medium image	
	    			db2.execute("INSERT INTO images (path_thumb, path_article, article_server_id, position, label, date_created, server_id) VALUES(?,?,?,?,?,?,?)", newThumbImageName, newMediumImageName, article.id, image.position, image.label, strToday, image.id );							

				}
				
			}
			
		} // End of Images Loop
		
		db2.close();
		
		return true;
		
	}
	
	
	function syncArticleCategories(categories, article){
		
		var db2 = Ti.Database.open('KateDb');
		
		var localCategories = {};
				        		
		// Search for existing Agreements
		var checkCategories = db2.execute("SELECT article_server_id, category_server_id, date_created FROM article_categories WHERE article_server_id = ?", article.id);
		
		
		// loop through the existing inventories if they exist
    	while (checkCategories.isValidRow()) {
    		localCategories[checkCategories.fieldByName("category_server_id")] = {};
    		localCategories[checkCategories.fieldByName("category_server_id")]['article_server_id'] = checkCategories.fieldByName("article_server_id");
    		localCategories[checkCategories.fieldByName("category_server_id")]['date_created'] = checkCategories.fieldByName("date_created");
    		checkCategories.next();
    	}
		
		checkCategories.close();
		
		// Loop through inbound data
		for (var cat_i=0; cat_i < categories.length; cat_i++){
			
			var category = categories[cat_i].category;
			
			
			// Check to see if the current inbound inventory exists in the local DB already
        	if ( _.has(localCategories, category.id) ) {
   
   	        			// Setup local & server dates to compare 
    			var serverDT 	= dp(category.updated_at); 
    			var localDT 	= dp(localCategories[category.id]['date_created']); 
    			
    			// if server version is newer than the local version, update
    			if ( serverDT > localDT ){
    				    				
    				db2.execute("UPDATE article_categories SET article_server_id=?, category_server_id=?, date_created=? WHERE category_server_id=? AND article_server_id=?", category.id, article.id, strToday, localCategories[category.id]['category_server_id'], localCategories[category.id]['article_server_id'] );
    			
    			}
    			
			} else {    			
				// SQL to add medium image	
    			db2.execute("INSERT INTO article_categories (article_server_id, category_server_id, date_created) VALUES(?,?,?)", article.id, category.id, strToday );							
    			
			}
			
		} // End of Categories Loop
		
		db2.close();
		
		return true;
		
	}
	
	
	function syncArticleLinks(links, article){
		
		var db2 = Ti.Database.open('KateDb');
		
		var localLinks = {};
				        		
		// Search for existing Agreements
		var checkLinks = db2.execute("SELECT server_id, date_created FROM links WHERE article_server_id = ?", article.id);
		
		
		// loop through the existing inventories if they exist
    	while (checkLinks.isValidRow()) {
    		localLinks[checkLinks.fieldByName("server_id")] = {};
    		localLinks[checkLinks.fieldByName("server_id")]['date_created'] = checkLinks.fieldByName("date_created");
    		checkLinks.next();
    	}
		
		checkLinks.close();
		// Loop through inbound data
		for (var link_i=0; link_i < links.length; link_i++){
			
			var link = links[link_i].link;
			
			if ( link.deleted_at != null ){	
					db2.execute("DELETE FROM images WHERE server_id = ?", link.id);
			} else {
			
				// Check to see if the current inbound inventory exists in the local DB already
	        	if ( _.has(localLinks, link.id) ) {
	   
	   	        			// Setup local & server dates to compare 
	    			var serverDT 	= dp(link.updated_at); 
	    			var localDT 	= dp(localLinks[link.id]['date_created']); 
	    			
	    			// if server version is newer than the local version, update
	    			if ( serverDT > localDT ){	    				
	    				db2.execute("UPDATE links SET url=?, link_label=?, date_created=? WHERE server_id=?", link.url, link.item_label, strToday, link.id );
	    			
	    			}else{
	    				//Ti.API.log("Link - Local version newer");
	    				// Do nothing
	    			}
	    			
				} else {
	    			// Inventory not local so can be inserted into system
	    			//Ti.API.log("Link - About to Insert");
	    			
					// SQL to add medium image	
	    			db2.execute("INSERT INTO links (url, server_id, article_server_id, link_label, date_created) VALUES(?,?,?,?,?)", link.url, link.id, article.id, link.item_label, strToday );							
	    			
				}
				
			}
			
		} // End of Links Loop
		
		db2.close();
		
		return true;
		
	}
	
	var dCount = 0;
	var dLength;
	
	function downloadFiles(files) {
		
		// Get thumbnails first
		if (thumbQueue.length > 0 ){
			doDownload(thumbQueue);
		}
		
		// Then get the bigger images
		if (fileQueue.length > 0 ){
			doDownload(fileQueue);
		}
		
		
		
	}
	
	
	function doDownload(files){
	
		// Check the fileQueue has items
		if ( files.length > 0 ) {
		
			var get_remote_file = function(filename, url, fn_end, fn_progress,i) {
		            var file_obj = {file:filename, url:url, path: null};

		                if ( Titanium.Network.online ){
		                    var c = Titanium.Network.createHTTPClient();
		                    
		                    c.enableKeepAlive = false;
		                    c.setTimeout(3000);
		                    c.onload = function() {
		                        if (c.status == 200 ) {
		                            var f = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory+'../Library/Caches/',filename);
		                            f.write(this.responseData);
		                            f.remoteBackup = false;
		                            
		                            file_obj.path = Titanium.Filesystem.applicationDataDirectory;
		                            fn_end(file_obj);
		                          
		                           if ( i == files.length-1 ) {
		                           		
		                           			if ( calledComplete == false){
		                           				Ti.App.fireEvent('sync_complete');
		                           			}
		                           			
		                           }
		                           
		                        } else {
		                            file_obj.error = 'file not found'; // to set some errors codes
		                            fn_end(file_obj);
		                        }
		                        
		                    };
		                    
		                    c.onreadystatechange = function(e) {
		                        if(e.readyState == 4) {
		                            fn_end(file_obj);
		                        }
		                    };
		                    
		                    c.ondatastream = function(e) {
		                        if ( fn_progress ) fn_progress(e.progress);
		                    };
		                    
		                    c.error = function(e) {
		                        file_obj.error = e.error;
		                        fn_end(file_obj);
		                        alert('Error with Download');
		                    };
		                    
		                    //Ti.API.log(url);
		                    
		                    c.open('GET',url);
		                    c.send();           
		                
		                } else {
		                    file_obj.error =  'no internet';
		                    fn_end(file_obj);
		                }
		           // }
		        };
				
				// Set dLength to number of downloads
				dLength = files.length;
				
				for ( var i=0; i<files.length; i++ ) {
					
					// assetServerUrl is set to point through the CDN 
					get_remote_file(
			            files[i][1],
			            assetServerUrl+files[i][0],
			            function(fileobj)
			            {
			                
			            }, 
			            function(progress)
			            {
			                //Ti.API.info(progress);
			            },
			            i
		        	);

				}
			
		} else {
			
			Ti.App.fireEvent("sync_complete");	
		}
			
	}
	
	var syncComplete = function(){
			
			switch(Ti.App.syncBack){
				
				case "endReloading":
					Ti.App.fireEvent("end_reloading");
					
					break;
				case "loadUI":
					Ti.App.fireEvent("loadUI");
					
					break;
				case "end_updating":
					Ti.App.fireEvent("end_updating");
					break;
				case "updateCategoryArticles":
					Ti.App.fireEvent("updateCategoryArticles");
					break;
				default:
					Ti.App.fireEvent("loadUI");
					
			}
			

		
	};
	
	// ============================================================//
	// ============================================================//
	// Event Listeners											   //
	// ============================================================//
	
	Ti.API.addEventListener("log_sync", logSync);
	Ti.API.addEventListener("full_sync", fullSync);
	Ti.App.addEventListener("sync_from", function(e){
		syncFrom(e.ts);
	});
	Ti.App.addEventListener("sync_category_from", function(e){
		syncCategoryFrom(e.cat, e.ts);
	});
	
	Ti.API.addEventListener("sync_categories", syncCategories);
	Ti.App.addEventListener("sync_complete", syncComplete);	
	
	
	