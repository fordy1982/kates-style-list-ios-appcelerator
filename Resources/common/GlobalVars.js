	
	var _ = require('common/underscore')._;

	var AppTabGroup = require('ui/AppTabGroup');
	
	//var Admob = require('ti.admob');
	
	var serverUrl = "http://www.katesstylelist.com";  
	var assetServerUrl = "http://cdn.katesstylelist.com"; 
	var siteCode = "ksl";
	
	var myApp = {};
	
	var osname = Ti.Platform.osname;
	
	var isIPhone = (osname=='iphone') ? true : false;
	var isIPad = (osname=='ipad') ? true : false;
	/*
	var ad = Admob.createView({
	    top: 0, left: 0,
	    width: 320, height: 50,
	    publisherId: 'a14fa65d799c3d7', // You can get your own at http: //www.admob.com/
	    adBackgroundColor: 'black',
	    testing: true,
	    dateOfBirth: new Date(1985, 10, 1, 12, 1, 1),
	    gender: 'female',
	    keywords: 'fashion'
	});
	*/
	
	
	//
	//  CREATE CUSTOM LOADING INDICATOR
	//
	/*
	var actInd = Titanium.UI.createActivityIndicator({
		height:		50,
		width:		50,
		top:		200,
		color:		"#333333",
		style:		Titanium.UI.iPhone.ActivityIndicatorStyle.DARK,
		message: 	"Loading"
	});
	*/
	
	function guidGenerator() {
	    var S4 = function() {
	       return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
	    };
	    return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
	}
	
	function todayGenerator() {
		
		// Setup today's date ready for insert
		var today 	= new Date();
		var year 	= today.getFullYear();
		var month 	= today.getMonth() + 1;
		
		// ensure month and day is 2 digit
		if (month < 10)
			month = "0"+month.toString();
		var day		= today.getDate();
		if ( day < 10 )
			day	= "0"+day.toString();
			
		var hour	= today.getHours();
		if ( hour < 10)
			hour = "0"+hour.toString();
		var minute	= today.getMinutes();
		if ( minute < 10)
			minute = "0"+minute.toString();
		var second	= today.getSeconds();
		
		if ( second < 10)
			second = "0"+second.toString();
		
		return year + "-" + month + "-" + day + "T" + hour + ":" + minute + ":" + second + "Z";
		
	}
	
	function reverseDate(dateStr, hideTime) {
		
		hideTime = hideTime;
		
		var datePieces;
		
		if ( dateStr.indexOf("T") != -1 ){
			// String is DateTime
			
			dateStr = dateStr.split("T");
			
			var datePart 	= dateStr[0];
			var timePart 	= dateStr[1]; 
			
			Ti.API.log(datePart);
			Ti.API.log(timePart);
			
			timeStr 		= timePart.substring(0,4);
			
			datePieces 		= datePart.split("-"); 
			
		}else{
			// String is just Date
			datePieces = dateStr.split("-");
		}
		
		str = datePieces[2] + "/" + datePieces[1] + "/" + datePieces[0];
		
		if ( hideTime != false ) {
			str = timeStr + str;
		}
		
		return str; 
		
	}
	
	function formatDate() {
		
		var d = new Date;
		var datestr = d.getMonth()+'/'+d.getDate()+'/'+d.getFullYear();
		if (d.getHours()>=12)
		{
	           datestr+=' '+(d.getHours()==12 ? 
	              d.getHours() : d.getHours()-12)+':'+
	              d.getMinutes()+' PM';
		} else {
			datestr+=' '+d.getHours()+':'+d.getMinutes()+' AM';
		}
		return datestr;
	}
	
	//
	// Add global event handlers to hide/show custom indicator
	//
	
	Titanium.App.addEventListener('show_indicator', function(e)
	{
		Ti.API.info("IN SHOW INDICATOR");
		//actInd.show();
	});
	Titanium.App.addEventListener('hide_indicator', function(e)
	{
		Ti.API.info("IN HIDE INDICATOR");
		actInd.hide();
	});
	
	// trap app shutdown event
	Titanium.App.addEventListener('close',function(e)
	{
		Ti.API.info("The application is being shutdown");
	});
	
	Array.prototype.contains = function (element) {
		for (var i = 0; i < this.length; i++) {
			if (this[i] == element) {
				return true;
			}
		}
		return false;
	};
	
	function dp(dateStr) {
	    var pattern = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})Z$/;
	    var match = pattern.exec(dateStr);
	    if (!match) {throw new Error('::Error, #dp could not parse dateStr '+dateStr);}
	    // we're safe to use the fields
	    return new Date(match[1], match[2]-1, match[3], match[4], match[5], match[6]);
	}


	
